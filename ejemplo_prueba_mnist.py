import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

#Initial values

width = 28 # width of the image in pixels
height = 28 # height of the image in pixels
flat = width * height # number of pixels in one image
class_output = 10 # number of possible classifications for the problem

# Creacion tf.placehloder para entradas y salidas

x= tf.placeholder(tf.float32,[None,784])
y_= tf.placeholder(tf.float32,[None, 10])


## Convertir  el input a una imagen de 28 x28

x_image= tf.reshape(x, shape=[-1, 28,28,1])

# -1 es para indicar que el tamaño del lote puede ser variable
# 28x28 es alto y ancho de la imagen
# 1 es el canala de la imagen que en este caso es una escala de grises



## Creaci[on de variables de la primera capa la cual será 32 filtros de 5x5 con un solo canal de escala de grises =1
## Lo que estamos haciendo es pasando de una profundidad de 1 a una de 32
## IMPORTANTE: en [5,5,1,32] estamos diciendo que vamos a pasar un filtro de 5x5 y que  la capa que tiene como entrada
## el valor de 1 de profundidad, tendrá una salida de 32 de profundidad

conv_w1=tf.Variable(tf.truncated_normal([5,5,1,32],stddev=0.01)) #a filter / kernel tensor of shape [filter_height, filter_width, in_channels, out_channels]. W is of size [5, 5, 1, 32]

conv_b1=tf.Variable(tf.constant(0.1,shape=[32]))

## Creación de capa de salia usando la función softmax que  encuentra la probabilidad

layer1=tf.nn.relu(tf.nn.conv2d(x_image,conv_w1,strides=[1,1,1,1],padding='SAME')+conv_b1)


# Hacemos el pooling de la primera capa
# ksize=[1,2,2,32] Significa que usamos un fltro de 2x2 y el resultado final será  que
## el ancho y el alto de la salidad se reducen a la mitad . pasa de 28x28 a 14x14. la profundidad es de 32
# strides=[1,2,2,1] significa que ess filtro se debe mover de 2 en dos para que no haya
# overlapping de información

convolucion1=tf.nn.max_pool(layer1,ksize=[1,2,2,1],strides=[1,2,2,1],padding='SAME')


## Creamos las variables para la segunda capa de convolución

## Vamos a pasar un filtro de 5x5 y pasaremos en un filtro de profundidad 32 a uno de profundidad 64

conv_w2= tf.Variable(tf.truncated_normal([5,5,32,64], stddev=0.01))
conv_b2= tf.Variable(tf.truncated_normal([64], stddev=0.01))

## Esto está para revisar
layer2= tf.nn.relu(tf.nn.conv2d(convolucion1,conv_w2,strides=[1,1,1,1], padding='SAME')+conv_b2)

## Ahora hacemos de nuevo un maxpooling que reduzca el area a la mitad usando un filtro de [2,2] y un desplazamiento
## de [2,2]

convolucion2= tf.nn.max_pool(layer2,ksize=[1,2,2,1],strides=[1,2,2,1], padding='SAME')


## Ahora hacemos una capa totalmente conectada

## Necesitas una capa totalmente conectada para usar el Softmax y crear las probabilidades al final. Las capas totalmente conectadas toman las imágenes filtradas de alto nivel de la capa anterior, es decir, las 64 métricas  de profundidad y las convierten en una matriz plana.

#Por lo tanto, cada matriz [7x7] se convertirá en una matriz de [49x1], y entonces toda la matriz 64 se conectará, lo que hace una matriz de tamaño [3136x1]. Lo conectaremos a otra capa de tamaño [1024x1]. Por lo tanto, el peso entre estas 2 capas será [3136x1024]


## Primero aplanamos la matriz de 7x7

layer2_matrix = tf.reshape(convolucion2, [-1, 7*7*64]) #7*7*64=3136


hidden_weight=tf.Variable(tf.truncated_normal([7*7*64,1024], stddev=0.01))

hidden_bias= tf.Variable(tf.truncated_normal([1024],stddev=0.01))

hidden_fc_layer=tf.nn.relu(tf.matmul(layer2_matrix,hidden_weight)+hidden_bias)

#Capa de abandono, fase opcional para reducir el sobreajuste u overfitting

#Es una fase en la que la red "olvida" algunas características. En cada paso de entrenamiento en un mini-lote, algunas unidades se desconectan aleatoriamente para que no interactúen con la red. Es decir, los pesos no pueden ser actualizados, ni afectan el aprendizaje de los otros nodos de la red. Esto puede ser muy útil para las redes neuronales muy grandes para evitar el sobreajuste del modelo.


keep_prob = tf.placeholder(tf.float32)
layer_drop = tf.nn.dropout(hidden_fc_layer, keep_prob)
layer_drop


## Finalmente la ultima capa completamente llena  con el resultados


weight=tf.Variable(tf.truncated_normal([1024,10], stddev=0.01))
bias=tf.Variable(tf.truncated_normal([10], stddev=0.01))



y= tf.nn.softmax(tf.matmul(layer_drop,weight)+bias)



## Para Recordar

#0) Input - MNIST dataset
#1) Convolutional and Max-Pooling
#2) Convolutional and Max-Pooling
#3) Fully Connected Layer
#4) Processing - Dropout
#5) Readout layer - Fully Connected
#6) Outputs - Classified digits



## Creacion de gradiente

cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))

# Funci[on de costo

optimizer= tf.train.AdamOptimizer(0.01).minimize(cross_entropy)

## Definimos la prediccion: Vamos a saber cuantos de esas predicciones están correctas

correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))


## Accuracy:

accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))


#sess.close()
sess= tf.InteractiveSession()
sess.run(tf.global_variables_initializer())


#sess.run(accuracy,feed_dict={x:mnist.train._images, y_: mnist.train._labels})


for i in range(250):
    batch = mnist.train.next_batch(50)
    if i%10 == 0:

        train_accuracy = accuracy.eval(feed_dict={x:batch[0], y_: batch[1], keep_prob: 1.0})
        print("step %d, training accuracy %g"%(i, float(train_accuracy)))
    optimizer.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})


## Evaluar el modelo con los datos de Testing

print("test accuracy %g"%accuracy.eval(feed_dict={x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0}))


## Visualizar los kernels


#kernels = sess.run(tf.reshape(tf.transpose(conv_w1, perm=[2, 3, 0,1]),[32,-1]))

