from utils_cuenta_producto import rec_cuenta_prod

import sys
import logging
import pyspark
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession

#python main.py 0.8 0.1 0.1 "pry_spsa" "datasets_prueba/local/2019_02_18_13_39_11" "research/codigo_cuenta_rec"

#gcloud dataproc jobs submit pyspark gs://pry_spsa/code/recomendacion_productos/preprocesamiento/main.py --py-files="gs://pry_spsa/code/recomendacion_productos/preprocesamiento/utils_cuenta_producto.py" --cluster=dprc-mkt-tensorflow-tony-jupyter --region=us-east1 -- 0.8 0.1 0.1 pry_spsa datasets_prueba/local/2019_02_18_13_39_11 output/2019-01-01/123000/recomendacion/cuenta rec_cuenta yarn 3G 1 32 40 True


logger = logging.getLogger()
logger.setLevel(logging.INFO)

# verifica parámetros
if len(sys.argv) == 1:
    logger.error("No se han recibido los parámetros de training_split, bucket, input_path, output_path")
    raise Exception("Parametros deben ser cadenas")

training_split = sys.argv[1]
testing_split = sys.argv[2]
eval_split = sys.argv[3]
bucket=sys.argv[4]
path_data_input=sys.argv[5]
path_data_output=sys.argv[6]
setAppName=sys.argv[7]
setMaster=sys.argv[8]
executor_memory=sys.argv[9]
executor_cores=sys.argv[10]
executor_instances=sys.argv[11]
cores_max=sys.argv[12]
logConf=sys.argv[13]


def create_sc(setAppName,setMaster,executor_memory,executor_cores,executor_instances,cores_max,logConf):
    sc_conf = pyspark.SparkConf()
    sc_conf.setAppName(setAppName)
    sc_conf.setMaster(setMaster)
    sc_conf.set('spark.executor.memory', executor_memory)
    sc_conf.set('spark.executor.cores', executor_cores)
    sc_conf.set('spark.executor.instances', executor_instances)
    sc_conf.set('spark.cores.max', cores_max)
    sc_conf.set('spark.logConf', logConf)
    print(sc_conf.getAll())

    sc = SparkContext(conf=sc_conf)

    return sc


spark_sess = SparkSession(create_sc(setAppName,setMaster,executor_memory,executor_cores,
                               executor_instances,cores_max,logConf))



Rec_cuenta_prod= rec_cuenta_prod(training_split, testing_split, eval_split, bucket, path_data_input,
                                 path_data_output, spark_sess)


# Lectura de datos

sdf=Rec_cuenta_prod.lectura_datos_bucket()
#creación de diccionarios
diccionario_cod ,diccionario_prod= Rec_cuenta_prod.creacion_diccionarios(sdf)
# Creación de rating binario

Rec_cuenta_prod.creacion_rating_binario(sdf, diccionario_cod ,diccionario_prod)

