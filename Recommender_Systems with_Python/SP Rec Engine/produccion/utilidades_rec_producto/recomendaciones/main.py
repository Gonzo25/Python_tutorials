from utilidades_recomendacion import crear_recomendaciones

import sys
import logging
import pyspark
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession

#python main.py 0.8 0.1 0.1 "pry_spsa" "datasets_prueba/local/2019_02_18_13_39_11" "research/codigo_cuenta_rec"

#gcloud dataproc jobs submit pyspark gs://pry_spsa/code/recomendacion_productos/recomendaciones/main.py --py-files="gs://pry_spsa/code/recomendacion_productos/recomendaciones/utilidades_recomendacion.py" --cluster=dprc-mkt-tensorflow-tony-jupyter --region=us-east1 -- pry_spsa output/2019-01-01/123000/recomendacion/cuenta  4 4 rec_cuenta yarn 3G 1 32 40 True


logger = logging.getLogger()
logger.setLevel(logging.INFO)

# verifica parámetros
if len(sys.argv) == 1:
    logger.error("No se han recibido los parámetros de training_split, bucket, input_path")
    raise Exception("Parametros deben ser cadenas")



bucket=sys.argv[1]
input_path=sys.argv[2]
n_items=sys.argv[3]
n_users=sys.argv[4]
setAppName=sys.argv[5]
setMaster=sys.argv[6]
executor_memory=sys.argv[7]
executor_cores=sys.argv[8]
executor_instances=sys.argv[9]
cores_max=sys.argv[10]
logConf=sys.argv[11]

def create_sc(setAppName,setMaster,executor_memory,executor_cores,executor_instances,cores_max,logConf):
    sc_conf = pyspark.SparkConf()
    sc_conf.setAppName(setAppName)
    sc_conf.setMaster(setMaster)
    sc_conf.set('spark.executor.memory', executor_memory)
    sc_conf.set('spark.executor.cores', executor_cores)
    sc_conf.set('spark.executor.instances', executor_instances)
    sc_conf.set('spark.cores.max', cores_max)
    sc_conf.set('spark.logConf', logConf)
    print(sc_conf.getAll())

    sc = SparkContext(conf=sc_conf)

    return sc


spark_sess = SparkSession(create_sc(setAppName,setMaster,executor_memory,executor_cores,
                               executor_instances,cores_max,logConf))



Rec_cuenta_prod= crear_recomendaciones(bucket, input_path, n_items,n_users,spark_sess)


Rec_cuenta_prod.recomendaciones()