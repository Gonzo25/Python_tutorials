

from pyspark.ml.recommendation import ALS
from pyspark.sql.functions import explode, col, collect_set



class crear_recomendaciones:

    def __init__(self, bucket, input_path, n_items,n_users,spark_sess):

        self.bucket=bucket
        self.input_path=input_path
        self.n_items=n_items
        self.n_users=n_users
        self.spark_sess = spark_sess


    def recomendaciones(self):

        spark=self.spark_sess


        training_folder = "preprocesamiento/output_ratings/training"
        testing_folder = "preprocesamiento/output_ratings/testing"
        eval_folder= "preprocesamiento/output_ratings/evaluacion"
        grilla_folder="entrenamiento/output_grilla"
        user_folder="preprocesamiento/diccionarios/users"
        item_folder="preprocesamiento/diccionarios/items"
        type_rec_folder = "recomendacion/cuenta"
        type_rec_inv_folder = "recomendacion/inversa"

        # Creación de rutas

        training_binary_path = "gs://{}/{}/{}".format(self.bucket, self.input_path, training_folder)
        testing_binary_path = "gs://{}/{}/{}".format(self.bucket, self.input_path, testing_folder)
        eval_binary_path = "gs://{}/{}/{}".format(self.bucket, self.input_path, eval_folder)
        grilla_path = "gs://{}/{}/{}".format(self.bucket, self.input_path, grilla_folder)
        bucket_path_dict_cuenta_userId = "gs://{}/{}/{}".format(self.bucket, self.input_path, user_folder)
        bucket_path_dict_producto_itemId = "gs://{}/{}/{}".format(self.bucket, self.input_path, item_folder)
        path_recomendacion_cuenta_prod = "gs://{}/{}/{}".format(self.bucket, self.input_path, type_rec_folder)
        path_recomendacion_inversa_cuenta_prod = "gs://{}/{}/{}".format(self.bucket,
                                                                        self.input_path, type_rec_inv_folder)

        bucket_path_rating_training_binario = "{}/part-*.snappy.parquet".format(training_binary_path)
        bucket_path_rating_testing_binario = "{}/part-*.snappy.parquet".format(testing_binary_path)
        bucket_path_rating_eval_binario = "{}/part-*.snappy.parquet".format(eval_binary_path)
        #path_binary_pipeline_cv_codigo_cuenta_grilla = "{}/part-*.snappy.parquet".format(grilla_path)

        print(bucket_path_rating_training_binario)
        print(bucket_path_rating_testing_binario)
        print(bucket_path_rating_eval_binario)
        print(grilla_path)
        print(bucket_path_dict_cuenta_userId)
        print(bucket_path_dict_producto_itemId)
        print(path_recomendacion_cuenta_prod)
        print(path_recomendacion_inversa_cuenta_prod)


        ratings_train = spark.read.parquet(bucket_path_rating_training_binario)
        ratings_test = spark.read.parquet(bucket_path_rating_testing_binario)
        ratings_eval = spark.read.parquet(bucket_path_rating_eval_binario)

        df = ratings_train.union(ratings_test.union(ratings_eval))

        user_mapping = spark.read.parquet(bucket_path_dict_cuenta_userId)
        item_mapping = spark.read.parquet(bucket_path_dict_producto_itemId)

        # pipeline_binario =Pipeline.load("gs://pry_spsa/models/pipeline_als_model2")
        grilla = spark.read.csv(grilla_path, header=True)
        grilla2 = grilla.sort(col('areaUnderPR').desc()).first()

        best_alpha = float(grilla2['alpha'])
        best_rank = int(grilla2['rank'])
        best_regParam = float(grilla2['regParam'])
        best_maxIter = int(grilla2['maxIter'])

        als = ALS(maxIter=best_maxIter, regParam=best_regParam, alpha=best_alpha, rank=best_rank, userCol="userId",
                  itemCol="itemId",
                  ratingCol="rating", coldStartStrategy="drop", implicitPrefs=True, seed=625, nonnegative=True)

        als_model_binario = als.fit(df)

        # Creación de recomendación Cuanta- Producto

        itemRecs = als_model_binario.recommendForAllUsers(int(self.n_items))

        itemRecs_unnested = itemRecs.select(col('userId'), explode(col('recommendations')).alias('recomendaciones'))
        itemRecs_unnested.createOrReplaceTempView("itemRecs_unnested")

        itemRecs_unnested2 = itemRecs_unnested.select(['userId', 'recomendaciones.itemId', 'recomendaciones.rating'])
        itemRecs_unnested2.createOrReplaceTempView("itemRecs_unnested2")

        # Creamos vista para hacer joins que conviertan de cuenta y producto a userId e itemId

        user_mapping.createOrReplaceTempView("user_mapping")
        item_mapping.createOrReplaceTempView("item_mapping")

        conversion_to_cuenta_producto = '''Select b.codigo_cuenta, 
                                               c.categoria, 
                                               a.rating 
                                               from itemRecs_unnested2 a 
                                               join user_mapping b on a.userId= b.userId 
                                               join item_mapping c on a.itemId=c.itemId'''


        resultados_recomendacion = spark.sql(conversion_to_cuenta_producto)

        # Guardamos recomendaciones
        resultados_recomendacion.write.mode("overwrite").option("header", "true").parquet(
            path_recomendacion_cuenta_prod)


        # REcomendación inversa

        itemRecs = als_model_binario.recommendForAllItems(int(self.n_users))

        itemRecs_unnested = itemRecs.select(col('itemId'), explode(col('recommendations')).alias('recomendaciones'))
        itemRecs_unnested.createOrReplaceTempView("itemRecs_unnested")

        itemRecs_unnested2 = itemRecs_unnested.select(['itemId', 'recomendaciones.userId', 'recomendaciones.rating'])
        itemRecs_unnested2.createOrReplaceTempView("itemRecs_unnested2")

        # Creamos vista para hacer joins que conviertan de cuenta y producto a userId e itemId



        conversion_to_cuenta_producto_inv = '''Select b.codigo_cuenta, 
                                               c.categoria, 
                                               a.rating 
                                               from itemRecs_unnested2 a 
                                               join user_mapping b on a.userId= b.userId 
                                               join item_mapping c on a.itemId=c.itemId'''


        resultados_recomendacion_inversa = spark.sql(conversion_to_cuenta_producto_inv)

        resultados_recomendacion_inversa.write.mode("overwrite").option("header", "true").parquet(
            path_recomendacion_inversa_cuenta_prod)

        print("se generaron las recomendaciones de subfamilias en: {} ".format(path_recomendacion_cuenta_prod))
        print("se generaron las recomendaciones inversas  de subfamilias en: {}".format(path_recomendacion_inversa_cuenta_prod))

        return(0)


