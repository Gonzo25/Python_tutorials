
from pyspark.mllib.evaluation import  RankingMetrics

from pyspark.sql.functions import  col, collect_set

import pandas as pd

from pyspark.ml.recommendation import ALS
import re
from pyspark.ml import Pipeline
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder
from pyspark.ml.evaluation import BinaryClassificationEvaluator
from pyspark.ml.feature import VectorAssembler
from datetime import datetime





class cross_validation_subfam:

    def __init__(self, bucket, input_path, alpha_list,rank_list,maxIter_list,
                 regParam_list,k_folds,n_items,spark_sess):

        self.bucket=bucket
        self.input_path=input_path
        self.alpha_list=alpha_list
        self.rank_list=rank_list
        self.maxIter_list=maxIter_list
        self.regParam_list=regParam_list
        self.k_folds=k_folds
        self.n_items=n_items
        self.spark_sess = spark_sess


    def entrenamiento_als(self):
        spark = self.spark_sess

        training_folder = "preprocesamiento/output_ratings/training"
        testing_folder = "preprocesamiento/output_ratings/testing"
        output_grilla_path = "entrenamiento/output_grilla"

        training_binary_path = "gs://{}/{}/{}".format(self.bucket, self.input_path, training_folder)
        testing_binary_path = "gs://{}/{}/{}".format(self.bucket, self.input_path, testing_folder)
        path_binary_pipeline_cv_codigo_cuenta_grilla = "gs://{}/{}/{}/".format(self.bucket, self.input_path,
                                                                               output_grilla_path)

        path_binary_rating_training_userId_itemId = "{}/part-*.snappy.parquet".format(training_binary_path)
        path_binary_rating_testing_userId_itemId = "{}/part-*.snappy.parquet".format(testing_binary_path)
        print(path_binary_rating_training_userId_itemId)
        print(path_binary_rating_testing_userId_itemId)

        ratings_train = spark.read.parquet(path_binary_rating_training_userId_itemId)

        #ratings_test = spark.read.parquet(path_binary_rating_testing_userId_itemId)

        initial_date = datetime.strftime(datetime.today(), "%Y_%m_%d_%H_%M_%S")
        print(initial_date)

        feature_columns = ratings_train.columns[:-1]
        assembler = VectorAssembler(inputCols=feature_columns, outputCol='features')

        als_implicit = ALS(userCol="userId", itemCol="itemId", ratingCol="rating", coldStartStrategy="drop",
                           implicitPrefs=True, seed=625)

        pipeline = Pipeline(stages=[assembler, als_implicit])
        # Diseña la grilla de parametros a probar


        # convertimos los strings a listas numéricas

        rank_string = re.sub("[[]|[]]", "", self.rank_list)
        rank_list_str= re.split(",", rank_string)
        rank_list1 = [int(i) for i in rank_list_str]

        alpha_string = re.sub("[[]|[]]", "", self.alpha_list)
        alpha_list_string = re.split(",", alpha_string)
        alpha_list1 = [float(i) for i in alpha_list_string]

        maxIter_string = re.sub("[[]|[]]", "", self.maxIter_list)
        maxIter_list_str = re.split(",", maxIter_string)
        maxIter_list1 = [int(i) for i in maxIter_list_str]

        regParam_string = re.sub("[[]|[]]", "", self.regParam_list)
        regParam_list_str = re.split(",", regParam_string)
        regParam_list1 = [float(i) for i in regParam_list_str]





        paramGrid = ParamGridBuilder() \
            .addGrid(als_implicit.rank, rank_list1) \
            .addGrid(als_implicit.regParam, regParam_list1) \
            .addGrid(als_implicit.alpha, alpha_list1) \
            .addGrid(als_implicit.maxIter, maxIter_list1) \
            .build()

        # Set evaluator
        ## usamos el evaluador ninario que se encuentra en la librería pyspar.ml.evaluation
        # http://spark.apache.org/docs/2.4.0/api/python/_modules/pyspark/ml/evaluation.html

        # modelEvaluator = RegressionEvaluator(predictionCol="features", labelCol="rating", metricName="rmse")
        modelEvaluator = BinaryClassificationEvaluator(rawPredictionCol='features', labelCol="rating",
                                                       metricName="areaUnderPR")

        crossval = CrossValidator(estimator=pipeline,
                                  estimatorParamMaps=paramGrid,
                                  evaluator=modelEvaluator,
                                  numFolds=int(self.k_folds))

        # Perform cross-validation

        cvModel = crossval.fit(ratings_train)

        # Select best model and get its parameters
        best_als_model = cvModel.bestModel.stages[1]

        # best_als_model = cvModel
        print("Best number of latent factors (rank parameter): " + str(best_als_model._java_obj.parent().getRank()))
        print("Best value of regularization factor: " + str(best_als_model._java_obj.parent().getRegParam()))
        print("Max Iterations: " + str(best_als_model._java_obj.parent().getMaxIter()))
        print("Best value of alpha: " + str(best_als_model._java_obj.parent().getAlpha()))

        final_date = datetime.strftime(datetime.today(), "%Y_%m_%d_%H_%M_%S")
        print(final_date)

        # salvamos el paramteros
        params = [{p.name: v for p, v in m.items()} for m in cvModel.getEstimatorParamMaps()]

        parameter_df = pd.DataFrame.from_dict([
            {cvModel.getEvaluator().getMetricName(): metric, **ps}
            for ps, metric in zip(params, cvModel.avgMetrics)
        ])

        # guardamos resultados de la grilla
        parameter_df_spark = spark.createDataFrame(parameter_df)
        parameter_df_spark.coalesce(1).write.mode("overwrite").format('csv').\
            save(path_binary_pipeline_cv_codigo_cuenta_grilla, header='true')


        #print(type(best_als_model))

        return (0)



    def precision_at_k(self):

        spark=self.spark_sess

        training_folder = "preprocesamiento/output_ratings/training"
        testing_folder = "preprocesamiento/output_ratings/testing"
        output_grilla_path = "entrenamiento/output_grilla"
        precision_path = "entrenamiento/metricas"
        path_precision_n = "gs://{}/{}/{}/".format(self.bucket, self.input_path, precision_path)

        path_binary_rating_training_userId_itemId = "gs://{}/{}/{}".format(self.bucket, self.input_path,
                                                                           training_folder)
        path_binary_rating_testing_userId_itemId = "gs://{}/{}/{}".format(self.bucket, self.input_path, testing_folder)
        path_binary_pipeline_cv_codigo_cuenta_grilla = "gs://{}/{}/{}/".format(self.bucket, self.input_path,
                                                                               output_grilla_path)

        ratings_train = spark.read.parquet(path_binary_rating_training_userId_itemId)
        ratings_test = spark.read.parquet(path_binary_rating_testing_userId_itemId)

        grilla = spark.read.csv(path_binary_pipeline_cv_codigo_cuenta_grilla, header=True)

        grilla2 = grilla.sort(col('areaUnderPR').desc()).first()

        best_alpha = float(grilla2['alpha'])
        best_rank = int(grilla2['rank'])
        best_regParam = float(grilla2['regParam'])
        best_maxIter = int(grilla2['maxIter'])

        als_model = ALS(maxIter=best_maxIter, regParam=best_regParam, alpha=best_alpha,
                             rank=best_rank, userCol="userId", itemCol="itemId",
                             ratingCol="rating", coldStartStrategy="drop", implicitPrefs=True,
                             seed=625, nonnegative=True)

        best_als_model = als_model.fit(ratings_train.union(ratings_test))


        # Creamos las recomendaciones en training

        users_train = ratings_train.select(als_model.getUserCol())
        recomendaciones_train = best_als_model.recommendForUserSubset(users_train, int(self.n_items))
        recomendaciones_train.createOrReplaceTempView("recomendaciones_training")
        recomendaciones_train_df = spark.sql(
            'select distinct userId, recommendations.itemId prediction from recomendaciones_training')
        recomendaciones_train_df.createOrReplaceTempView("recomendaciones_train_df")


        # Creamos las recomendaciones en testing

        users_test = ratings_test.select(als_model.getUserCol())
        recomendaciones_testing = best_als_model.recommendForUserSubset(users_test, int(self.n_items))
        recomendaciones_testing.createOrReplaceTempView("recomendaciones_testing")
        recomendaciones_testing_df = spark.sql(
            'select distinct userId, recommendations.itemId prediction from recomendaciones_testing')

        recomendaciones_testing_df.createOrReplaceTempView("recomendaciones_testing_df")

        # Creamos el dataframe de prediction y label para training y testing
        # La idea es poder crear dos columnas donde aparecen los productos recomendados y los comprados para cada cuenta
        # training

        train_comprados = ratings_train.groupby("userId").agg(collect_set("itemId").alias('label'))
        train_comprados.createOrReplaceTempView("train_comprados")

        test_comprados = ratings_test.groupby("userId").agg(collect_set("itemId").alias('label'))
        test_comprados.createOrReplaceTempView("test_comprados")

        query_join_nested_list_train = '''select b.prediction, a.label
                                  from train_comprados a join recomendaciones_train_df b on a.userId=b.userId
        '''
        query_join_nested_list_test = '''select b.prediction, a.label
                                  from test_comprados a join recomendaciones_testing_df b on a.userId=b.userId
        '''

        # Aqui tengo el dataframe de artículos ocmprados y recomendados tanto en entrenamiento como en testeo

        predictionAndLabels_train = spark.sql(query_join_nested_list_train)
        predictionAndLabels_test = spark.sql(query_join_nested_list_test)

        # Instantiate metrics object

        metrics_ranking_train = RankingMetrics(predictionAndLabels_train.rdd)
        metrics_ranking_test = RankingMetrics(predictionAndLabels_test.rdd)

        # Creamos Dataframe de precisiones en training y testeo

        ## Creación de lista con el número de recomendaciones a ejecutar
        fibonacci = [1, 2, 3, 5, 8, 13, 21, 25, 30, 34, 40, 45, 55, 89, 144, 233, 377, 610, 987]
        cantidad_recomendaciones = [i for i in fibonacci if i < int(self.n_items)] + [int(self.n_items)]

        predicciones = list()

        for n in cantidad_recomendaciones:
            predicciones.append((n, metrics_ranking_train.precisionAt(n), metrics_ranking_test.precisionAt(n)))

        predicciones_n = pd.DataFrame(predicciones, columns=['cantidad_rec', 'precision_train', 'precision_test'])
        predicciones_n = predicciones_n.dropna()

        print(predicciones_n.to_string)
        predicciones_n = spark.createDataFrame(predicciones_n)
        #predicciones_n.to_csv(path_binary_pipeline_cv_codigo_cuenta_grilla+"/predicciones_n")
        predicciones_n.coalesce(1).write.mode("overwrite").format('csv').save(path_precision_n,header = 'true')

        print("best alpha {}".format(best_alpha))
        print("best rank {}".format(best_rank))
        print("best regparam {}".format(best_regParam))
        print("best maxiter {}".format(best_maxIter))
        return(0)

