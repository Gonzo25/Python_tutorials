


from pyspark.sql.functions import size, count, mean, stddev, udf, min, max, col, desc, asc,  lit, concat_ws, to_date, unix_timestamp, collect_list
#%matplotlib inline
from pyspark.sql.types import StringType,  IntegerType
from pyspark.ml.fpm import FPGrowth



class crear_recomendaciones:

    def __init__(self, bucket, input_path, output_path,spark_sess):

        self.bucket=bucket
        self.input_path=input_path
        self.output_path = output_path
        self.spark_sess = spark_sess


    def lectura_datos_bucket(self):
        bucket_path = "gs://{}/{}".format(self.bucket, self.input_path)

        parquet_files = "{}/part-*.snappy.parquet".format(bucket_path)

        spark=self.spark_sess



        sdf = spark.read.parquet(parquet_files)

        # Cambio a enteros los datos de fechas
        # hice una modificación desde la ingesta que falta por probar

        sdf = sdf.withColumn('anio_calendario', sdf.anio_calendario.cast(IntegerType()))
        sdf = sdf.withColumn('nro_mes_anio', sdf.nro_mes_anio.cast(IntegerType()))
        sdf = sdf.withColumn('nro_dia_mes', sdf.nro_dia_mes.cast(IntegerType()))
        sdf = sdf.withColumn('nro_dia_anio', sdf.nro_dia_anio.cast(IntegerType()))
        sdf = sdf.withColumn('nro_semana_anio', sdf.nro_semana_anio.cast(IntegerType()))
        sdf = sdf.withColumn('trimestre_calendario', sdf.trimestre_calendario.cast(IntegerType()))
        sdf = sdf.withColumn('semestre_calendario', sdf.semestre_calendario.cast(IntegerType()))
        # sdf = sdf.withColumn('fecha',concat(col('nro_dia_mes'),col('nro_mes_anio'),col('anio_calendario'))).cast(DateType())

        sdf = sdf.withColumn("concat", concat_ws("-", col("anio_calendario"), col("nro_mes_anio"), col("nro_dia_mes")))
        sdf = sdf.withColumn("fecha", to_date(unix_timestamp(col("concat"), "yyyy-MM-dd").cast("timestamp")))
        sdf = sdf.drop("concat")

        sdf = sdf.select("codigo_cuenta", "anio_calendario", "nro_mes_anio", "codigo_producto")

        return(sdf)

    def creacion_lift_table(self,sdf):


        #type_rec_folder = "recomendacion/lift_cliente_nuevo"
        path_lift_cliente_nuevo = "gs://{}/{}/".format(self.bucket, self.output_path)


        gdf = sdf.groupBy("codigo_cuenta", "anio_calendario", "nro_mes_anio", "codigo_producto").agg(count("codigo_producto"))
        ndf = gdf.groupBy("codigo_cuenta", "anio_calendario", "nro_mes_anio").agg(
            collect_list("codigo_producto").alias("items"))

        transacciones = ndf.count()
        fpGrowth = FPGrowth(itemsCol="items", minSupport=0.01, minConfidence=0.01)
        model = fpGrowth.fit(ndf)
        frecuencias = model.freqItemsets
        soportes = frecuencias.withColumn('soporte', col('freq') / transacciones)
        reglas = model.associationRules
        fdf = soportes.join(reglas, col('items') == col('consequent')).withColumn("lift",
                                                                                  col('confidence') / col('soporte'))
        fdf = fdf.select('antecedent', 'consequent', 'lift')
        fdf = fdf.withColumn('antecedent_len', size(col("antecedent")))
        fdf = fdf.withColumn('consecuent_len', size(col("consequent")))
        fdf = fdf.drop('antecedent_len', 'consecuent_len').filter(
            'antecedent_len == 1 and consecuent_len == 1 and lift > 1')

        unlist_udf = udf(lambda x: list(x)[0], StringType())

        fdf1 = fdf.withColumn('antecedent', unlist_udf(col('antecedent')))
        fdf1 = fdf1.withColumn('consequent', unlist_udf(col('consequent')))

        fdf1.write.mode("overwrite").option("header", "true").parquet(path_lift_cliente_nuevo)













