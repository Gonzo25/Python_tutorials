from utilidades_recomendacion import crear_recomendaciones

import sys
import logging
import pyspark
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession

#python main.py 0.8 0.1 0.1 "pry_spsa" "datasets_prueba/local/2019_02_18_13_39_11" "research/codigo_cuenta_rec"

#gcloud dataproc jobs submit pyspark gs://pry_spsa/code/recomendacion_clientes_nuevos/recomendaciones_nuevos_clientes/main.py --py-files="gs://pry_spsa/code/recomendacion_clientes_nuevos/recomendaciones_nuevos_clientes/utilidades_recomendacion.py" --cluster=dprc-mkt-tensorflow-tony-jupyter --region=us-east1 -- pry_spsa datasets_prueba/local/2019_02_18_13_39_11 output/2019-01-01/123000/recomendacion/clientes_nuevos/ rec_cuenta yarn 3G 1 32 40 True


logger = logging.getLogger()
logger.setLevel(logging.INFO)

# verifica parámetros
if len(sys.argv) == 1:
    logger.error("No se han recibido los parámetros de training_split, bucket, input_path")
    raise Exception("Parametros deben ser cadenas")



bucket=sys.argv[1]
input_path=sys.argv[2]
output_path=sys.argv[3]
setAppName=sys.argv[4]
setMaster=sys.argv[5]
executor_memory=sys.argv[6]
executor_cores=sys.argv[7]
executor_instances=sys.argv[8]
cores_max=sys.argv[9]
logConf=sys.argv[10]

def create_sc(setAppName,setMaster,executor_memory,executor_cores,executor_instances,cores_max,logConf):
    sc_conf = pyspark.SparkConf()
    sc_conf.setAppName(setAppName)
    sc_conf.setMaster(setMaster)
    sc_conf.set('spark.executor.memory', executor_memory)
    sc_conf.set('spark.executor.cores', executor_cores)
    sc_conf.set('spark.executor.instances', executor_instances)
    sc_conf.set('spark.cores.max', cores_max)
    sc_conf.set('spark.logConf', logConf)
    print(sc_conf.getAll())

    sc = SparkContext(conf=sc_conf)

    return sc


spark_sess = SparkSession(create_sc(setAppName,setMaster,executor_memory,executor_cores,
                               executor_instances,cores_max,logConf))



Rec_clientes_nuevos= crear_recomendaciones(bucket, input_path, output_path ,spark_sess)


sdf= Rec_clientes_nuevos.lectura_datos_bucket()
Rec_clientes_nuevos.creacion_lift_table(sdf)