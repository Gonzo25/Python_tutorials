import logging
from pyspark.sql.functions import size, count, udf,mean, stddev, when, min, max, col, desc, asc, concat, lit, concat_ws, to_date, unix_timestamp, collect_list, collect_set,regexp_replace
from pyspark.sql.types import StringType, FloatType, IntegerType, LongType, DateType, StructType, StructField, ArrayType

import random

# Log de errores
logger = logging.getLogger()
logger.setLevel(logging.INFO)


class rec_clientes_nuevos:

    def __init__(self,training_split,
                 bucket,
                 path_data_input,
                 path_data_output,spark_sess):

        self.training_split=training_split
        self.bucket=bucket
        self.path_data_input=path_data_input
        self.path_data_output=path_data_output
        self.spark_sess=spark_sess


    def lectura_datos_bucket(self):
        bucket_path = "gs://{}/{}".format(self.bucket, self.path_data_input)

        parquet_files = "{}/part-*.snappy.parquet".format(bucket_path)

        spark=self.spark_sess



        sdf = spark.read.parquet(parquet_files)

        # Cambio a enteros los datos de fechas
        # hice una modificación desde la ingesta que falta por probar


        sdf = sdf.withColumn('anio_calendario', sdf.anio_calendario.cast(IntegerType()))
        sdf = sdf.withColumn('nro_mes_anio', sdf.nro_mes_anio.cast(IntegerType()))
        sdf = sdf.withColumn('nro_dia_mes', sdf.nro_dia_mes.cast(IntegerType()))
        sdf = sdf.withColumn('nro_dia_anio', sdf.nro_dia_anio.cast(IntegerType()))
        sdf = sdf.withColumn('nro_semana_anio', sdf.nro_semana_anio.cast(IntegerType()))
        sdf = sdf.withColumn('trimestre_calendario', sdf.trimestre_calendario.cast(IntegerType()))
        sdf = sdf.withColumn('semestre_calendario', sdf.semestre_calendario.cast(IntegerType()))


        sdf = sdf.withColumn("concat", concat_ws("-", col("anio_calendario"), col("nro_mes_anio"), col("nro_dia_mes")))
        sdf = sdf.withColumn("fecha", to_date(unix_timestamp(col("concat"), "yyyy-MM-dd").cast("timestamp")))
        sdf = sdf.drop("concat")

        sdf = sdf.select("codigo_cuenta", "anio_calendario", "nro_mes_anio", "codigo_producto")

        return(sdf)

    def creacion_diccionarios(self, sdf):

        spark = self.spark_sess

        type_data = "preprocesamiento/diccionarios/users"
        bucket_path_diccionario_userId = "gs://{}/{}/{}/".format(self.bucket,
                                                                 self.path_data_output,
                                                                 type_data)
        type_data =  "preprocesamiento/diccionarios/items"
        bucket_path_dict_cat_itemId = "gs://{}/{}/{}/".format(self.bucket,
                                                                 self.path_data_output,
                                                                 type_data)


        # creamos Vdiccionarios de mapping


        sdf.createOrReplaceTempView("sdf")



        user_mapping = spark.sql('''Select  distinct codigo_cuenta from sdf''')
        item_mapping = spark.sql('''Select  distinct codigo_producto from sdf''')

        cols = user_mapping.columns
        user_mapping = user_mapping.rdd.zipWithIndex().map(lambda row: (row[1],) + tuple(row[0])).toDF(["userId"] + cols)
        cols1 = item_mapping.columns
        item_mapping = item_mapping.rdd.zipWithIndex().map(lambda row: (row[1],) + tuple(row[0])).toDF(["itemId"] + cols1)


        # Guardamos diccionarios para usarlos
        user_mapping.write.mode('overwrite').option("header", "true").parquet(bucket_path_diccionario_userId)
        item_mapping.write.mode('overwrite').option("header", "true").parquet(bucket_path_dict_cat_itemId)

        print(bucket_path_diccionario_userId)
        print(bucket_path_dict_cat_itemId)

        return user_mapping, item_mapping

    def creacion_dataset(self, sdf,user_mapping, item_mapping):
        spark = self.spark_sess
        training_split = float(self.training_split)

        user_mapping.createOrReplaceTempView("user_mapping")
        item_mapping.createOrReplaceTempView("item_mapping")
        sdf.createOrReplaceTempView("sdf")

        # guardamos nuestros ratings en la carpeta

        training_folder_name = "preprocesamiento/output_ratings/training"

        path_cold_start_training = "gs://{}/{}/{}/".format(self.bucket,
                                                                      self.path_data_output,
                                                                      training_folder_name)


        join_user_id_item_id = '''Select b.userId, c.itemId, a.anio_calendario, a.nro_mes_anio
                                     from sdf a join user_mapping b on a.codigo_cuenta= b.codigo_cuenta 
                                     join item_mapping c on a.codigo_producto=c.codigo_producto'''

        df = spark.sql(join_user_id_item_id)
        gdf = df.groupBy("userId", "anio_calendario", "nro_mes_anio", "itemId").agg(
            count("itemId").alias('cantidad_items'))
        gdf = gdf.withColumn("itemId", gdf["itemId"].cast(IntegerType()))
        gdf = gdf.withColumn("userId", gdf["userId"].cast(IntegerType()))
        ndf = gdf.groupBy("userId", "anio_calendario", "nro_mes_anio").agg(
            collect_set("itemId").alias("listado_items"))
        ndf = ndf.filter(size(col('listado_items')) > 2)

        def split_df(lista, training_split):
            num_part = round(len(lista) * training_split)
            lista_train = random.sample(lista, k=num_part)
            return lista_train

        split_udf = udf(lambda x: split_df(x, training_split), ArrayType(IntegerType()))
        differencer_udf = udf(lambda x, y: list(set(x) - set(y)), ArrayType(IntegerType()))

        ndf1 = ndf.withColumn('train_list', split_udf(col('listado_items')))
        ndf2 = ndf1.withColumn('label', differencer_udf(col('listado_items'), col('train_list')))
        ndf2.write.mode("overwrite").option("header", "true").parquet(path_cold_start_training)
