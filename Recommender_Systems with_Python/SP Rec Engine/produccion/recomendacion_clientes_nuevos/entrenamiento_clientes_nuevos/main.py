from utils_training_clientes_nuevos import cross_validation_clientes_nuevos
import sys
import logging

import pyspark
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession


logger = logging.getLogger()
logger.setLevel(logging.INFO)


# gcloud dataproc jobs submit pyspark gs://pry_spsa/code/recomendacion_clientes_nuevos/entrenamiento_clientes_nuevos/main.py --py-files="gs://pry_spsa/code/recomendacion_clientes_nuevos/entrenamiento_clientes_nuevos/utils_training_clientes_nuevos.py" --cluster=dprc-mkt-tensorflow-tony-jupyter --region=us-east1 -- pry_spsa "output/2019-01-01/123000/recomendacion/clientes_nuevos" 0.8 "[0.0,0.01,0.02, 0.03]" "[0.0,0.01,0.02, 0.03]"  rec_cuenta yarn 3G 1 32 40 True


# verifica parámetros
if len(sys.argv) == 1:
    logger.error("No se han recibido los parámetros para hacer entrenamiento como la ruta de los datos,"
                 " la ruta donde guardar la grilla y el nombre del bucket")
    raise Exception("Parametros deben ser cadenas")


bucket = sys.argv[1]
input_path = sys.argv[2]
training_split=sys.argv[3]
support_list= sys.argv[4]
confidence_list= sys.argv[5]
setAppName=sys.argv[6]
setMaster=sys.argv[7]
executor_memory=sys.argv[8]
executor_cores=sys.argv[9]
executor_instances=sys.argv[10]
cores_max=sys.argv[11]
logConf=sys.argv[12]


def create_sc(setAppName,setMaster,executor_memory,executor_cores,executor_instances,cores_max,logConf):
    sc_conf = pyspark.SparkConf()
    sc_conf.setAppName(setAppName)
    sc_conf.setMaster(setMaster)
    sc_conf.set('spark.executor.memory', executor_memory)
    sc_conf.set('spark.executor.cores', executor_cores)
    sc_conf.set('spark.executor.instances', executor_instances)
    sc_conf.set('spark.cores.max', cores_max)
    sc_conf.set('spark.logConf', logConf)
    print(sc_conf.getAll())

    sc = SparkContext(conf=sc_conf)
    #print(sc.version)


    return sc


spark_sess = SparkSession(create_sc(setAppName,setMaster,executor_memory,executor_cores,
                               executor_instances,cores_max,logConf))
#print(spark_sess.version)
validacion_cruzada = cross_validation_clientes_nuevos(bucket,input_path ,training_split,
                                          support_list,confidence_list,spark_sess)

validacion_cruzada.entrenamiento_fp_growth()

#validacion_cruzada.precision_at_k()