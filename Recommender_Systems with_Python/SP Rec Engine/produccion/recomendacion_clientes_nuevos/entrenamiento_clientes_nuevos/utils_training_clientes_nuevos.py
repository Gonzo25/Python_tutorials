
from pyspark.mllib.evaluation import  RankingMetrics

from pyspark.sql.functions import  col, collect_set,  size
from pyspark.mllib.evaluation import  RankingMetrics, MulticlassMetrics, MultilabelMetrics
from pyspark.sql.types import  FloatType
from pyspark.ml.fpm import FPGrowth

import re
from datetime import datetime





class cross_validation_clientes_nuevos:

    def __init__(self, bucket, input_path,training_split, support_list,confidence_list,spark_sess):

        self.bucket=bucket
        self.input_path=input_path
        self.training_split=training_split
        self.support_list=support_list
        self.confidence_list=confidence_list
        self.spark_sess = spark_sess


    def entrenamiento_fp_growth(self):
        spark = self.spark_sess

        training_folder = "preprocesamiento/output_ratings/training"

        output_grilla_path = "entrenamiento/output_grilla"

        training_binary_path = "gs://{}/{}/{}".format(self.bucket, self.input_path, training_folder)

        path_grilla_modelo = "gs://{}/{}/{}/".format(self.bucket, self.input_path,
                                                                               output_grilla_path)

        path_binary_rating_training_userId_itemId = "{}/part-*.snappy.parquet".format(training_binary_path)

        print(path_binary_rating_training_userId_itemId)


        ratings_df = spark.read.parquet(path_binary_rating_training_userId_itemId)


        initial_date = datetime.strftime(datetime.today(), "%Y_%m_%d_%H_%M_%S")
        print(initial_date)

        training_split=float(self.training_split)

        train, validate = ratings_df.randomSplit([training_split, 1 - training_split], seed=0)

        # convertimos los strings a listas numéricas

        support_string = re.sub("[[]|[]]", "", self.support_list)
        support_string_str= re.split(",", support_string )
        minSupport_list = [float(i) for i in support_string_str]

        confidence_string = re.sub("[[]|[]]", "", self.confidence_list)
        confidence_list_string = re.split(",", confidence_string)
        minConfidence_list = [float(i) for i in confidence_list_string]



        # Building 5 folds within the training set.
        test1, test2, test3, test4, test5 = train.randomSplit([0.2, 0.2, 0.2, 0.2, 0.2], seed=625)
        train1 = test2.union(test3).union(test4).union(test5)
        train2 = test3.union(test4).union(test5).union(test1)
        train3 = test4.union(test5).union(test1).union(test2)
        train4 = test5.union(test1).union(test2).union(test3)
        train5 = test1.union(test2).union(test3).union(test4)

        precision_optimal = 0


        for support in minSupport_list:
            for conf in minConfidence_list:

                fpGrowth = FPGrowth(itemsCol="train_list", minSupport=support, minConfidence=conf)

                # fit models
                model1 = fpGrowth.fit(train1)
                model2 = fpGrowth.fit(train2)
                model3 = fpGrowth.fit(train3)
                model4 = fpGrowth.fit(train4)
                model5 = fpGrowth.fit(train5)



                pred_test1 = model1.transform(test1)
                pred_test2 = model2.transform(test2)
                pred_test3 = model3.transform(test3)
                pred_test4 = model4.transform(test4)
                pred_test5 = model5.transform(test5)



                predictionAndLabels_test1 = pred_test1.filter(size(col('prediction'))>0)
                predictionAndLabels_test2 = pred_test2.filter(size(col('prediction'))>0)
                predictionAndLabels_test3 = pred_test3.filter(size(col('prediction'))>0)
                predictionAndLabels_test4 = pred_test4.filter(size(col('prediction'))>0)
                predictionAndLabels_test5 = pred_test5.filter(size(col('prediction'))>0)

                if predictionAndLabels_test1.count()>0:
                    predictionAndLabels_test1 = predictionAndLabels_test1.select(['prediction', 'label'])
                    metrics_test1 = RankingMetrics(predictionAndLabels_test1.rdd)
                    precision_prom1 = metrics_test1.meanAveragePrecision
                else:
                    precision_prom1 = 0.0

                if predictionAndLabels_test2.count()>0:
                    predictionAndLabels_test2 = predictionAndLabels_test2.select(['prediction', 'label'])
                    metrics_test2 = RankingMetrics(predictionAndLabels_test2.rdd)
                    precision_prom2 = metrics_test2.meanAveragePrecision
                else:
                    precision_prom2 = 0.0

                if predictionAndLabels_test3.count()>0:
                    predictionAndLabels_test3 = predictionAndLabels_test3.select(['prediction', 'label'])
                    metrics_test3 = RankingMetrics(predictionAndLabels_test3.rdd)
                    precision_prom3 = metrics_test3.meanAveragePrecision
                else:
                    precision_prom3 = 0.0

                if predictionAndLabels_test4.count()>0:
                    predictionAndLabels_test4 = predictionAndLabels_test4.select(['prediction', 'label'])
                    metrics_test4 = RankingMetrics(predictionAndLabels_test4.rdd)
                    precision_prom4 = metrics_test4.meanAveragePrecision
                else:
                    precision_prom4 = 0.0

                if predictionAndLabels_test5.count()>0:
                    predictionAndLabels_test5 = predictionAndLabels_test5.select(['prediction', 'label'])
                    metrics_test5 = RankingMetrics(predictionAndLabels_test5.rdd)
                    precision_prom5 = metrics_test5.meanAveragePrecision
                else:
                    precision_prom5 = 0.0


                promedio = (precision_prom1 + precision_prom2 + precision_prom3 + precision_prom4 + precision_prom5) / 5

                if promedio > precision_optimal:
                    precision_optimal = promedio
                    print('precision promedio: {}'.format(precision_optimal))
                    support_best = support
                    conf_best = conf

        lista_parametros = [support_best, conf_best]

        df_param = spark.createDataFrame(lista_parametros, FloatType())
        df_param = df_param.withColumnRenamed('value', 'parametros')
        df_param.coalesce(1).write.mode("overwrite").option("header", "true").parquet(path_grilla_modelo)
        print(path_grilla_modelo)


