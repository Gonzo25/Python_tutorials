import logging

# Log de errores
logger = logging.getLogger()
logger.setLevel(logging.INFO)


class rec_cuenta_prod_subfam:

    def __init__(self,training_split,
                 testing_split,
                 eval_split,bucket,
                 path_data_input,
                 path_data_output,spark_sess):

        self.training_split=training_split
        self.testing_split =testing_split
        self.eval_split=eval_split
        self.bucket=bucket
        self.path_data_input=path_data_input
        self.path_data_output=path_data_output
        self.spark_sess=spark_sess





    def lectura_datos_bucket(self):
        bucket_path = "gs://{}/{}".format(self.bucket, self.path_data_input)

        parquet_files = "{}/part-*.snappy.parquet".format(bucket_path)

        spark=self.spark_sess



        sdf = spark.read.parquet(parquet_files)


        return(sdf)

    def creacion_diccionarios(self, sdf):

        spark = self.spark_sess

        type_data = "preprocesamiento/diccionarios/users"
        bucket_path_diccionario_userId = "gs://{}/{}/{}/".format(self.bucket,
                                                                 self.path_data_output,
                                                                 type_data)
        type_data =  "preprocesamiento/diccionarios/items"
        bucket_path_dict_cat_itemId = "gs://{}/{}/{}/".format(self.bucket,
                                                                 self.path_data_output,
                                                                 type_data)


        # creamos Vdiccionarios de mapping

        sdf.createOrReplaceTempView("sdf")



        user_mapping = spark.sql('''Select  distinct cluster from sdf''')
        cat_mapping = spark.sql('''Select  distinct categoria from sdf''')

        cols = user_mapping.columns
        user_mapping = user_mapping.rdd.zipWithIndex().map(lambda row: (row[1],) + tuple(row[0])).toDF(["userId"] + cols)
        cols1 = cat_mapping.columns
        cat_mapping = cat_mapping.rdd.zipWithIndex().map(lambda row: (row[1],) + tuple(row[0])).toDF(["itemId"] + cols1)


        # Guardamos diccionarios para usarlos
        user_mapping.write.mode('overwrite').option("header", "true").parquet(bucket_path_diccionario_userId)
        cat_mapping.write.mode('overwrite').option("header", "true").parquet(bucket_path_dict_cat_itemId)

        print(bucket_path_diccionario_userId)
        print(bucket_path_dict_cat_itemId)

        return user_mapping, cat_mapping

    def creacion_rating_binario(self, sdf,user_mapping, cat_mapping):
        spark = self.spark_sess

        user_mapping.createOrReplaceTempView("user_mapping")
        cat_mapping.createOrReplaceTempView("cat_mapping")
        sdf.createOrReplaceTempView("sdf")

        # guardamos nuestros ratings en la carpeta

        training_folder_name = "preprocesamiento/output_ratings/training"
        testing_folder_name = "preprocesamiento/output_ratings/testing"
        eval_folder_name = "preprocesamiento/output_ratings/evaluacion"


        bucket_path_rating_training_binario = "gs://{}/{}/{}/".format(self.bucket,
                                                                      self.path_data_output,
                                                                      training_folder_name)
        bucket_path_rating_testing_binario = "gs://{}/{}/{}/".format(self.bucket,
                                                                     self.path_data_output,
                                                                     testing_folder_name)
        bucket_path_rating_eval_binario = "gs://{}/{}/{}/".format(self.bucket,
                                                                  self.path_data_output,
                                                                  eval_folder_name)

        training_split= float(self.training_split)
        testing_split = float(self.testing_split)
        eval_split= float(self.eval_split)

        query_venta_bruta = '''select cluster, categoria, 
                             sum(venta_bruta) tot_venta_bruta, 
                             log(sum(venta_bruta)) log_tot_venta_bruta
                             from sdf group by cluster, categoria'''

        venta_bruta = spark.sql(query_venta_bruta)
        venta_bruta.createOrReplaceTempView("venta_bruta")

        query_binary_rating = '''select cluster, categoria, 
                                 case when log_tot_venta_bruta > 0 then 1 else 0 end as rating
                                 from venta_bruta'''

        binary_rating = spark.sql(query_binary_rating)
        binary_rating.createOrReplaceTempView("binary_rating")

        ## Hacemos el join para poder entrenar con user_id e item_id en vez de cluster y categoría

        total_join_user_id_item_id = '''Select b.userId, c.itemId, a.rating 
                                     from binary_rating a join user_mapping b on a.cluster= b.cluster 
                                     join cat_mapping c on a.categoria=c.categoria'''

        total_binary_df = spark.sql(total_join_user_id_item_id)

        total_binary_df.createOrReplaceTempView("total_binary_df")

        train_user_binary, test_user_binary, eval_user_binary = total_binary_df.randomSplit(
            [training_split, testing_split, eval_split], 625)  # 490373 unicos cluster

        # creamos dos tablas para poder filtrar la data de acuerdo a los codigos cuentas que se haya usado en training y testing
        train_user_binary.createOrReplaceTempView("train_user_binary")
        test_user_binary.createOrReplaceTempView("test_user_binary")
        eval_user_binary.createOrReplaceTempView("eval_user_binary")

        binary_training_query = '''select * from total_binary_df
                             where userId in (select distinct userId from train_user_binary) '''

        binary_testing_query = '''select * from total_binary_df
                             where userId in (select distinct userId from test_user_binary) '''

        binary_eval_query = '''select * from total_binary_df
                          where userId in (select distinct userId from eval_user_binary)'''

        binary_training_df = spark.sql(binary_training_query)
        binary_testing_df = spark.sql(binary_testing_query)
        binary_eval_df = spark.sql(binary_eval_query)

        # guardamos los archivos en formato parquet
        binary_training_df.write.mode("overwrite").option("header", "true").parquet(bucket_path_rating_training_binario)
        binary_testing_df.write.mode("overwrite").option("header", "true").parquet(bucket_path_rating_testing_binario)
        binary_eval_df.write.mode("overwrite").option("header", "true").parquet(bucket_path_rating_eval_binario)

        print(bucket_path_rating_training_binario)
        print(bucket_path_rating_testing_binario)
        print(bucket_path_rating_eval_binario)

        return(0)

