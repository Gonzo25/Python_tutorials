from utils_training_cluster_subfam import cross_validation_subfam
import sys
import logging

import pyspark
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession


logger = logging.getLogger()
logger.setLevel(logging.INFO)


# gcloud dataproc jobs submit pyspark gs://pry_spsa/code/Recomendacion_cluster_subfamilias/entrenamiento_cluster/main.py --py-files="gs://pry_spsa/code/Recomendacion_cluster_subfamilias/entrenamiento_cluster/utils_training_cluster_subfam.py" --cluster=dprc-mkt-tensorflow-tony-jupyter --region=us-east1 -- pry_spsa "output/2019-01-01/123000/recomendacion/cluster_subfamilias" "[1.0]" "[4]" "[4]" "[0.9]" 2 5 rec_cuenta yarn 3G 1 32 40 True


# verifica parámetros
if len(sys.argv) == 1:
    logger.error("No se han recibido los parámetros para hacer entrenamiento como la ruta de los datos,"
                 " la ruta donde guardar la grilla y el nombre del bucket")
    raise Exception("Parametros deben ser cadenas")


bucket = sys.argv[1]
input_path = sys.argv[2]
alpha_list= sys.argv[3]
rank_list= sys.argv[4]
maxIter_list= sys.argv[5]
regParam_list= sys.argv[6]
k_folds= sys.argv[7]
n_items= sys.argv[8]
setAppName=sys.argv[9]
setMaster=sys.argv[10]
executor_memory=sys.argv[11]
executor_cores=sys.argv[12]
executor_instances=sys.argv[13]
cores_max=sys.argv[14]
logConf=sys.argv[15]


def create_sc(setAppName,setMaster,executor_memory,executor_cores,executor_instances,cores_max,logConf):
    sc_conf = pyspark.SparkConf()
    sc_conf.setAppName(setAppName)
    sc_conf.setMaster(setMaster)
    sc_conf.set('spark.executor.memory', executor_memory)
    sc_conf.set('spark.executor.cores', executor_cores)
    sc_conf.set('spark.executor.instances', executor_instances)
    sc_conf.set('spark.cores.max', cores_max)
    sc_conf.set('spark.logConf', logConf)
    print(sc_conf.getAll())

    sc = SparkContext(conf=sc_conf)

    return sc


spark_sess = SparkSession(create_sc(setAppName,setMaster,executor_memory,executor_cores,
                               executor_instances,cores_max,logConf))

validacion_cruzada = cross_validation_subfam(bucket,input_path ,
                                           alpha_list,rank_list,maxIter_list,regParam_list,k_folds,n_items,spark_sess)

validacion_cruzada.entrenamiento_als()

validacion_cruzada.precision_at_k()