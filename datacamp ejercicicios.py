# pegar dos strings

'los'+'enteros'

savings = 100

result = 100 * 1.10 ** 7
print("I started with $" + str(savings) + " and now have $" + str(result) + ". Awesome!")


type(savings)## sirve para saber el tipo de variable (str en R)

# Create a list 
areas=[hall,kit,liv,bed,bath]

# Adapt list areas
areas = ["hallway",hall, "kitchen",kit, "living room", liv, "bedroom",bed, "bathroom", bath]

# area variables (in square meters)
hall = 11.25
kit = 18.0
liv = 20.0
bed = 10.75
bath = 9.50

# house information as list of lists
house = [["hallway", hall],
         ["kitchen", kit],
         ["living room", liv],
         ["bedroom",bed],
         ["bathroom",bath]]
         
x = list["a", "b", "c", "d"]
x[1] ; x[-3] # same result!    
         
# Create the areas list
areas = ["hallway", 11.25, "kitchen", 18.0, "living room", 20.0, "bedroom", 10.75, "bathroom", 9.50]         
         


#my_list[start:end]
#The start index will be included, while the end index is not.


## Subsetting lists of a list

x = [["a", "b", "c"],
     ["d", "e", "f"],
     ["g", "h", "i"]]
x[2][0]
x[2][:2]



# install packages
#python3 get-pip.py
pip3 install numpy
# parece que no se necesita porque viene incluido en anaconda

import numpy as np
import pandas as pd
import pip
help(pip)


# packages
#Suppose you want to use the function inv(), 
#which is in the linalg subpackage of the scipy package. You want to be able
#to use this function as follows:

from scipy.linalg import inv as my_inv
my_inv([[1,2], [3,4]])

## Dictionaries

# Definition of dictionary
europe = {'spain':'madrid', 'france':'paris', 'germany':'berlin', 'norway':'oslo' }

# Print out the keys in europe

print(europe.keys())

# Print out value that belongs to key 'norway'
print(europe["norway"])





