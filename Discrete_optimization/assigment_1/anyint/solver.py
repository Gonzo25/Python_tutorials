#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np

def solve_it(input_data):
    # return a positive integer, as a string
    result = np.random.randint(low=-10, high=10)
    return str(result)

if __name__ == '__main__':
    print('This script submits the integer: %s\n' % solve_it(''))

