#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
from pyutilib.services import register_executable, registered_executable
register_executable( name='glpsol')
from pyomo.environ import *
from pyomo.opt import TerminationCondition
import numpy as np
import pandas as pd

os.chdir('C:/Users/gonzalo.moreno/Documents/Python_tutorials/Discrete_optimization/coloring/data/')
input_data_file=open('gc_4_1')
input_data = input_data_file.read()
input_data_file.close()


def color_it(df_x,col_dict):
    # si no aparece el color en el diccionario de colores asigne 0 porque es un nuevo nodo
    try:
        col_number=col_dict[df_x['node'].values[0]]
    except:
        col_number = 0


    for i in df_x.shape[0]:
        col_number=col_number+1
        col_dict.append(col_number)
    return (col_dict)




def solve_it(input_data):
    # Modify this code to run your optimization algorithm

    # parse the input
    lines = input_data.split('\n')

    first_line = lines[0].split()
    node_count = int(first_line[0])
    edge_count = int(first_line[1])

    edges = []
    col_dict=dict()

    for i in range(1, edge_count + 1):
        line = lines[i]
        parts = line.split()
        edges.append((int(parts[0]), int(parts[1])))
        col_dict[int(parts[0])]=0


    df = pd.DataFrame(edges, columns=['node', 'edges'])
    list_nodos=df['node'].unique()

    for count in range(len(list_nodos)):
        df_x=df.loc[df['node'] == list_nodos[count]]


    # build a trivial solution
    # every node has its own color
    solution = range(0, node_count)

    # prepare the solution in the specified output format
    output_data = str(node_count) + ' ' + str(0) + '\n'
    output_data += ' '.join(map(str, solution))

    return output_data


import sys

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        with open(file_location, 'r') as input_data_file:
            input_data = input_data_file.read()
        print(solve_it(input_data))
    else:
        print('This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/gc_4_1)')

