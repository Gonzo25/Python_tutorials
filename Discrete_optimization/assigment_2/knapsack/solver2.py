#!/usr/bin/python
# -*- coding: utf-8 -*-

from pyutilib.services import register_executable, registered_executable
register_executable( name='glpsol')
from collections import namedtuple
import os
from pyomo.environ import *
from pyomo.opt import TerminationCondition


# the file is opened
#os.chdir('C:/Users/ProBook/Documents/Python_tutorials/Discrete_optimization/assigment_2/knapsack')
dir='C:/Users/gonzalo.moreno/Documents/Python_tutorials/Discrete_optimization/assigment_2/knapsack/data/'

input_data1= input('digit the file you want to open: ')
Item = namedtuple("Item", ['index', 'value', 'weight'])

def solve_it(input_data1):
    # Modify this code to run your optimization algorithm

    # parse the input

    input_data_file = open(os.path.join(dir, input_data1), 'r')
    input_data = input_data_file.read()
    input_data_file.close()



    lines = input_data.split('\n')

    firstLine = lines[0].split() # number of items in the file and capacity of the knapsack

    item_count = int(firstLine[0])
    capacity = int(firstLine[1])


    # Creation of dictionary
    index_item = []
    value_item = []
    weight_item = []
    items = []
    value_dict = dict()
    weight_dict = dict()

    for i in range(1, item_count + 1):
        line = lines[i]
        parts = line.split()
        items.append(Item(i - 1, int(parts[0]), int(parts[1])))
        index_item.append(i - 1)
        value_item.append(int(parts[0]))
        weight_item.append(int(parts[1]))
        ##creo un diccionario de value and item
        value_dict[index_item[i - 1]] = int(parts[0])
        weight_dict[index_item[i - 1]] = int(parts[1])

    ###########################################################33
    # Creation of pyomo model

    # set modelo
    modelo = ConcreteModel()

    # set  definitions

    # modelo.articulos= Set(initialize= weight_dict.keys(), doc='articulos')
    modelo.items = weight_dict.keys()

    # parameter

    # set variables
    # http://nbviewer.jupyter.org/github/Pyomo/PyomoGallery/blob/master/transport/transport.ipynb
    # https://relopezbriega.github.io/blog/2017/01/18/problemas-de-optimizacion-con-python/
    # https://www.osti.gov/servlets/purl/1376827

    modelo.x = Var(modelo.items, within=Binary)

    # We set constrain of capacity

    modelo.capacityconstrain = Constraint(expr=sum(weight_dict[i] * modelo.x[i] for i in modelo.items) <= capacity)

    # objetive function

    def objetivo(modelo):
        return sum(value_dict[n] * modelo.x[n]
                   for n in modelo.items)

    modelo.OBJ = Objective(rule=objetivo, sense=maximize)
    ## Accessing results

    opt = SolverFactory("glpk")

    results = opt.solve(modelo)

    results.write()

    print("Print values for all variables")
    taken = []
    for v in modelo.component_data_objects(Var):
        taken.append(v.value)
        print(str(v), v.value)

    optimality = 0
    if results.solver.termination_condition == TerminationCondition.optimal:
        optimality = 1

    # prepare the solution in the specified output format
    output_data = str(modelo.OBJ()) + ' ' + str(optimality) + '\n'
    output_data += ' '.join(map(str, taken))

    # prepare the solution in the specified output format
    return taken

print(solve_it(input_data1))

