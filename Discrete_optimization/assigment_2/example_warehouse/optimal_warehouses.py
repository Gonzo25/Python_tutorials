
from pyutilib.services import register_executable, registered_executable
register_executable( name='glpsol')
from pyomo.environ import *
from pyomo.opt import TerminationCondition


N = 3 # Number of candidates o possible  warehouses
M = 4 # Number of customers
P = 3 # Number of chosen warehouses

# d{n,m} Cost of serving customer m from warehouse location n
d = {(1, 1): 1.7, (1, 2): 7.2, (1, 3): 9.0, (1, 4): 8.3,
     (2, 1): 2.9, (2, 2): 6.3, (2, 3): 9.8, (2, 4): 0.7,
     (3, 1): 4.5, (3, 2): 4.8, (3, 3): 4.2, (3, 4): 9.3}

model = ConcreteModel()
# SET
# Total amount of locations
model.Locations = range(1,N)
# Total amount of customers
model.Customers = range(1,M)

# VARABLES

# variable between 0 and 1 used to determine what proportion of demand will be provided for each warehouse.
model.x = Var(model.Locations, model.Customers, bounds=(0.0, 1.0))

# Binary variable that decide which warehouse will be chosen
model.y = Var(model.Locations, within=Binary)


# CONSTRAIN
# Constrain1: To guarantee all customers served)
model.single_x = ConstraintList()
for m in model.Customers:
    model.single_x.add(sum(model.x[n, m] for n in model.Locations) == 1.0)

# constrain2 that relates two variable trough a constrain that guarantee
# customer 𝑛 can only be served from warehouse 𝑚 if warehouse 𝑚 is selected)

model.bound_y = ConstraintList()
for n in model.Locations:
    for m in model.Customers:
        model.bound_y.add(model.x[n, m] <= model.y[n])


# select 𝑃 warehouses
model.num_facilities = Constraint(expr=sum(model.y[n] for n in model.Locations) <= P)

# OBJECTIVE FUNCTION

#minimizes the total cost of serving all customers 𝑀 where 𝑑𝑛,𝑚 is the cost of serving customer 𝑚
#from warehouse location 𝑛.

model.obj = Objective(expr=sum(d[n, m] * model.x[n, m] for n in model.Locations for m in model.Customers), sense=minimize)


## aprendiendo a accesar a los resultados

opt = SolverFactory("glpk")

results = opt.solve(model)

results.write()

print("Print values for all variables")
taken = []
for v in model.component_data_objects(Var):
    taken.append(v.value)
    print(str(v), v.value)

optimality = 0
if results.solver.termination_condition == TerminationCondition.optimal:
    optimality = 1
