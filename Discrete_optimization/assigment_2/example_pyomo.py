## ejemplos de pyomo acerca de odelos de optimización

from collections import namedtuple
import os
from pyutilib.services import register_executable, registered_executable
register_executable( name='glpsol')


os.chdir('C:/Users/ProBook/Documents/Python_tutorials/Discrete_optimization/assigment_2/knapsack')
Item = namedtuple("Item", ['index', 'value', 'weight'])

input_data_file= open("./data/ks_4_0",'r')
input_data = input_data_file.read()

    # aqui es donde se encuentra el primer problema

lines = input_data.split('\n')

firstLine = lines[0].split()
item_count = int(firstLine[0])
capacity = int(firstLine[1])

#Estos los cree yo
index_item = []
value_item=[]
weight_item=[]
items=[]
value_dict= dict()
weight_dict= dict()



for i in range(1, item_count+1):
    line = lines[i]
    parts = line.split()
    items.append(Item(i - 1, int(parts[0]), int(parts[1])))
    index_item.append(i-1)
    value_item.append(int(parts[0]))
    weight_item.append(int(parts[1]))
    ##creo un diccionario de value and item
    value_dict[index_item[i - 1]] = int(parts[0])
    weight_dict[index_item[i - 1]] = int(parts[1])




###########################################################33
# empezamos modelos pyomo
from pyomo.environ import *
from pyomo.opt import TerminationCondition


#set modelo
modelo= ConcreteModel()

#set  definitions

#modelo.articulos= Set(initialize= weight_dict.keys(), doc='articulos')
modelo.articulos= weight_dict.keys()

# parameter


#set variables
#http://nbviewer.jupyter.org/github/Pyomo/PyomoGallery/blob/master/transport/transport.ipynb
#https://relopezbriega.github.io/blog/2017/01/18/problemas-de-optimizacion-con-python/
#https://www.osti.gov/servlets/purl/1376827

modelo.x= Var(modelo.articulos, within=Binary)

# definimos restriccion de capacidad q

modelo.capacityconstrain = Constraint(expr=sum(weight_dict[i] *modelo.x[i] for i in modelo.articulos) <= capacity)

# función objetivo

def objetivo(modelo):
    return sum(value_dict[n] * modelo.x[n]
               for n in modelo.articulos)

modelo.OBJ = Objective(rule=objetivo, sense=maximize)


## aprendiendo a accesar a los resultados


opt = SolverFactory("glpk")

results = opt.solve(modelo)

results.write()

print("Print values for all variables")
taken=[]
for v in modelo.component_data_objects(Var):
    taken.append(v.value)
    print (str(v), v.value)


optimality=0
if results.solver.termination_condition == TerminationCondition.optimal:
    optimality=1

# prepare the solution in the specified output format
output_data = str(modelo.OBJ()) + ' ' + str(optimality) + '\n'
output_data += ' '.join(map(str, taken))

