## Case of study of hacking statistics usin python

#1)
# Compute ECDFs
x_1975, y_1975 = ecdf(bd_1975)
x_2012, y_2012 = ecdf(bd_2012)

# Plot the ECDFs
_ = plt.plot(x_1975, y_1975, marker='.', linestyle='none')
_ = plt.plot(x_2012, y_2012, marker='.', linestyle='none')

# Set margins
plt.margins(0.02)

# Add axis labels and legend
_ = plt.xlabel('beak depth (mm)')
_ = plt.ylabel('ECDF')
_ = plt.legend(('1975', '2012'), loc='lower right')

# Show the plot
plt.show()


#2) Parameter estimates of beak depths: Estimate the difference of the mean beak 
# depth of the G. scandens samples from 1975 and 2012 and report a 95% confidence
# interval.

#Since in this exercise you will use the draw_bs_reps() function you wrote in 
#chapter 2, it may be helpful to refer back to it.

# Compute the difference of the sample means: mean_diff
mean_diff = np.mean(bd_2012)-np.mean(bd_1975)

# Get bootstrap replicates of means
bs_replicates_1975 = draw_bs_reps(bd_1975,np.mean,10000)
bs_replicates_2012 = draw_bs_reps(bd_2012, np.mean, 10000)

# Compute samples of difference of means: bs_diff_replicates
bs_diff_replicates = bs_replicates_2012- bs_replicates_1975

# Compute 95% confidence interval: conf_int
conf_int = np.percentile(bs_diff_replicates, [2.5, 97.5])

# Print the results
print('difference of means =', mean_diff, 'mm')
print('95% confidence interval =', conf_int, 'mm')


#3) Hypothesis test: Are beaks deeper in 2012?
# Your plot of the ECDF and determination of the confidence interval make it pretty
#clear that the beaks of G. scandens on Daphne Major have gotten deeper. But is it
#possible that this effect is just due to random chance? In other words, what is 
#the probability that we would get the observed difference in mean beak depth if 
#the means were the same?

#Be careful! The hypothesis we are testing is not that the beak depths come from the
#same distribution. For that we could use a permutation test. The hypothesis is that
#the means are equal. To perform this hypothesis test, we need to shift the two data
#sets so that they have the same mean and then use bootstrap sampling to compute 
#the difference of means.

# Compute mean of combined data set: combined_mean
combined_mean = np.mean(np.concatenate((bd_1975, bd_2012)))

# Shift the samples
bd_1975_shifted = bd_1975 -np.mean(bd_1975) + combined_mean
bd_2012_shifted = bd_2012 - np.mean(bd_2012) + combined_mean

# Get bootstrap replicates of shifted data sets
bs_replicates_1975 = draw_bs_reps(bd_1975_shifted, np.mean,10000)
bs_replicates_2012 = draw_bs_reps(bd_2012_shifted,np.mean, 10000)

# Compute replicates of difference of means: bs_diff_replicates
bs_diff_replicates = bs_replicates_2012 - bs_replicates_1975

# Compute the p-value
p = np.sum(bs_diff_replicates>= mean_diff) / len(bs_diff_replicates)

# Print p-value
print('p =', p)

#4) Linear regressions

#Perform a linear regression for both the 1975 and 2012 data. Then, perform pairs 
#bootstrap estimates for the regression parameters. Report 95% confidence intervals
#on the slope and intercept of the regression line.

#You will use the draw_bs_pairs_linreg() function you wrote back in chapter 2.

#As a reminder, its call signature is draw_bs_pairs_linreg(x, y, size=1), and it 
#returns bs_slope_reps and bs_intercept_reps.

# Compute the linear regressions
slope_1975, intercept_1975 = np.polyfit(bl_1975,bd_1975,1)
slope_2012, intercept_2012 = np.polyfit(bl_2012, bd_2012,1)

# Perform pairs bootstrap for the linear regressions
bs_slope_reps_1975, bs_intercept_reps_1975 = \
        draw_bs_pairs_linreg(bl_1975, bd_1975, size=1000)
bs_slope_reps_2012, bs_intercept_reps_2012 = \
        draw_bs_pairs_linreg(bl_2012, bd_2012, size=1000)

# Compute confidence intervals of slopes
slope_conf_int_1975 = np.percentile(bs_slope_reps_1975,[2.5, 97.5])
slope_conf_int_2012 = np.percentile(bs_slope_reps_2012, [2.5, 97.5])
intercept_conf_int_1975 = np.percentile(bs_intercept_reps_1975, [2.5, 97.5])

intercept_conf_int_2012 = np.percentile(bs_intercept_reps_2012, [2.5, 97.5])


# Print the results
print('1975: slope =', slope_1975,
      'conf int =', slope_conf_int_1975)
print('1975: intercept =', intercept_1975,
      'conf int =', intercept_conf_int_1975)
print('2012: slope =', slope_2012,
      'conf int =', slope_conf_int_2012)
print('2012: intercept =', intercept_2012,
      'conf int =', intercept_conf_int_2012)
      

#5) Displaying the linear regression results


#Now, you will display your linear regression results on the scatter plot, the 
#code for which is already pre-written for you from your previous exercise. To do
#this, take the first 100 bootstrap samples (stored in bs_slope_reps_1975, 
#bs_intercept_reps_1975, bs_slope_reps_2012, and bs_intercept_reps_2012) and plot
#the lines with alpha=0.2 and linewidth=0.5 keyword arguments to plt.plot(). 



# Make scatter plot of 1975 data
_ = plt.plot(bl_1975, bd_1975, marker='.',
             linestyle='none', color='blue', alpha=0.5)

# Make scatter plot of 2012 data
_ = plt.plot(bl_2012, bd_2012, marker='.',
             linestyle='none', color='red', alpha=0.5)

# Label axes and make legend
_ = plt.xlabel('beak length (mm)')
_ = plt.ylabel('beak depth (mm)')
_ = plt.legend(('1975', '2012'), loc='upper left')

# Generate x-values for bootstrap lines: x
x = np.array([10, 17])

# Plot the bootstrap lines
for i in range(100):
    plt.plot(x, bs_slope_reps_1975[i]*x +bs_intercept_reps_1975[i],
             linewidth=0.5, alpha=0.2, color='blue')
    plt.plot(x, bs_slope_reps_2012[i]*x+ bs_intercept_reps_2012[i],
             linewidth=0.5, alpha=0.2, color='red')

# Draw the plot again
plt.show()








