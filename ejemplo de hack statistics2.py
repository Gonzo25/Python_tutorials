## Ejemplo de Hack Statistics with Python Statisticla Thinking2

import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets

iris= datasets.load_iris() ## Como cargar un data set

## Ejemplo Básico de como se usa data  real para hacer hack statistics
## La idea es plotear la ECDF teórica y real y ver si calzan. Cuando calzan 
## podemos confiar en la simulación
## ECDF: Experimental Cumulative Density Function



# Seed random number generator
np.random.seed(42)

# Compute mean no-hitter time: tau
tau = np.mean(nohitter_times)

# Draw out of an exponential distribution with parameter tau: inter_nohitter_time
inter_nohitter_time = np.random.exponential(tau, 100000)

# Plot the PDF and label axes
_ = plt.hist(inter_nohitter_time,bins=50, normed=True, histtype='step')
_ = plt.xlabel('Games between no-hitters')
_ = plt.ylabel('PDF')

# Show the plot
plt.show()


####################################################################################
####### Ejemplo de Hack Statistics con ECDF fuction ################################


x, y = ecdf(nohitter_times)

# Create a CDF from theoretical samples: x_theor, y_theor
x_theor, y_theor = ecdf(inter_nohitter_time)

# Overlay the plots
plt.plot(x_theor, y_theor)
plt.plot(x,y, marker='.', linestyle='none')

# Margins and axis labels
plt.margins(0.02)
plt.xlabel('Games between no-hitters')
plt.ylabel('CDF')

# Show the plot
plt.show()
#####################################################################################
# como generar un bootstraping  de los datos usando un loop para hacer simulacion
###################################################################################

for i in range(50):
    # Generate bootstrap sample: bs_sample
    bs_sample = np.random.choice(rainfall, size=len(rainfall))

    # Compute and plot ECDF from bootstrap sample
    x, y = ecdf(bs_sample)
    _ = plt.plot(x, y, marker='.', linestyle='none',
                 color='gray', alpha=0.1)

# Compute and plot ECDF from original data
x, y = ecdf(rainfall)
_ = plt.plot(x, y, marker='.')

# Make margins and label axes
plt.margins(0.02)
_ = plt.xlabel('yearly rainfall (mm)')
_ = plt.ylabel('ECDF')

# Show the plot
plt.show()

#############################################################################
####### Función que aplica una función a unos datos generados por bootstrap
#############################################################################

def bootstrap_replicate_1d(data, func):
    return func(np.random.choice(data, size=len(data)))
    
############################################################################
### Función que genera replicas desde el  data set original
##de una función aplicada a unos datos generados por bootstrap ##############

def draw_bs_reps(data, func, size=1):
    """Draw bootstrap replicates."""

    # Initialize array of replicates: bs_replicates
    bs_replicates = np.empty(size)

    # Generate replicates
    for i in range(size):
        bs_replicates[i] = bootstrap_replicate_1d(data, func)

    return bs_replicates

## Con boostraping uno puede generar un intervalo de confianza 
##de cualquier estadística usando la función np.percentile ################
## En el siguiente ejemplo veremos un codigo para generar el intervalo de 
## confianza de la varianza de los datos. Para ello debemos averiguar el valor def
## esa varianza usando bootstraping


# Generate 10,000 bootstrap replicates of the variance: bs_replicates
bs_replicates = draw_bs_reps(rainfall, np.var, 10000)

# Put the variance in units of square centimeters
bs_replicates=bs_replicates/100

# Make a histogram of the results
_ = plt.hist(bs_replicates, bins=50, normed=True)
_ = plt.xlabel('variance of annual rainfall (sq. cm)')
_ = plt.ylabel('PDF')

# Show the plot
plt.show()


## Función para generar bootstraping a una regresión lineal


def draw_bs_pairs_linreg(x, y, size=1):
    """Perform pairs bootstrap for linear regression."""

    # Set up array of indices to sample from: inds
    inds = np.arange(len(x))

    # Initialize replicates: bs_slope_reps, bs_intercept_reps
    bs_slope_reps = np.empty(size)
    bs_intercept_reps = np.empty(size)

    # Generate replicates
    for i in range(size):
        bs_inds = np.random.choice(inds, size=len(inds))
        bs_x, bs_y = x[bs_inds], y[bs_inds]
        bs_slope_reps[i], bs_intercept_reps[i] = np.polyfit(bs_x,bs_y,1)

    return bs_slope_reps, bs_intercept_reps

# Generate replicates of slope and intercept using pairs bootstrap
bs_slope_reps, bs_intercept_reps = draw_bs_pairs_linreg(illiteracy,fertility, 1000)

# Compute and print 95% CI for slope
print(np.percentile(bs_slope_reps, [2.5, 97.5]))

# Plot the histogram
_ = plt.hist(bs_slope_reps, bins=50, normed=True)
_ = plt.xlabel('slope')
_ = plt.ylabel('PDF')
plt.show()

###############################################################################
####### función para plotearmuchas gráficas hechas por bootstraping
################################################################################

# Generate array of x-values for bootstrap lines: x
x = np.array([0,100])

# Plot the bootstrap lines
for i in range(100):
    _ = plt.plot(x, bs_slope_reps[i]*x + bs_intercept_reps[i],
                 alpha=0.2, linewidth=0.5, color='red')

# Plot the data
_ = plt.margins(0.02)

# Label axes, set the margins, and show the plot
_ = plt.xlabel('illiteracy')
_ = plt.ylabel('fertility')
plt.margins(0.02)
plt.show()

################################################################################
##  Función de permutación para pruebas de hipotesis ###########################
###############################################################################

# Esta función permite generar replicas por bootstraping cuando uno usa como hipotesis nula
## que dos muestras son identicas. La idea es juntarlas, revolverlas aleatoriamente y asi ver
##si la diferencia de medias se mantiene

def permutation_sample(data1, data2):
    """Generate a permutation sample from two data sets."""

    # Concatenate the data sets: data
    data = np.concatenate((data1, data2))

    # Permute the concatenated array: permuted_data
    permuted_data = np.random.permutation(data)

    # Split the permuted array into two: perm_sample_1, perm_sample_2
    perm_sample_1 = permuted_data[:len(data1)]
    perm_sample_2 = permuted_data[len(data1):]

    return perm_sample_1, perm_sample_2
    
 ###############################################################################
 ## Función para generar una prueba estadística usando bootstraping ############
 ## Para ello usaremos la función anterior llamada : permutation_sample
 
 def draw_perm_reps(data_1, data_2, func, size=1):
    """Generate multiple permutation replicates."""

    # Initialize array of replicates: perm_replicates
    perm_replicates = np.empty(size)

    for i in range(size):
        # Generate permutation sample
        perm_sample_1, perm_sample_2 = permutation_sample(data_1,data_2)

        # Compute the test statistic
        perm_replicates[i] = func(perm_sample_1,perm_sample_2)

    return perm_replicates
    
## Esta función que acabamos de escribir se puede usar por ejemplo para
## pruebas estadísticas de diferencia de meidas
   
#############################################################################
### Función para la creación de diferencia de medias 

def diff_of_means(data_1, data_2):
    """Difference in means of two arrays."""

    # The difference of means of data_1, data_2: diff
    diff = np.mean(data_1)-np.mean(data_2)

    return diff

# Compute difference of mean impact force from experiment: empirical_diff_means
empirical_diff_means = diff_of_means(force_a, force_b)

# Draw 10,000 permutation replicates: perm_replicates
perm_replicates = draw_perm_reps(force_a, force_b,diff_of_means, size=10000)

# Compute p-value: p
p = np.sum(perm_replicates >= empirical_diff_means) / len(perm_replicates)

# Print the result
print('p-value =', p)

############################################################################
## PROCEDIMIENTO PARA PRUEBAS DE HIPOTESIS CON BOOTSTRAPING Y HACK STATISTICS
##############################################################################

# 1) Establecer la hipotesis nula a probar: Diferencia de medias? las dos distribuciones
## son identicas??

#2) Definir la hipotesis nula a probar. (Se asume que la Ho es verdadera)

#3) Se genera muchos datos simulados asumiendo que la hipotesis es verdadera

##4) Computar la prueba estadística para cada dato simulado

#5) el p-value es la fracción de los datos SIMULADOS para los cuales los datos
## son tan extremos como los datos REALES. Si es un valor muy bajo se rechaza la Ho
## Si es un valor alto entonces no se puede rechazar la hipotesis nula


##################################################################################
### Bootstrap de dos muestras para prueba estadística de diferencia de medias

#A two-sample bootstrap hypothesis test for difference of means.

#You performed a one-sample bootstrap hypothesis test, which is impossible to do 
#with permutation. Testing the hypothesis that two samples have the same 
#distribution may be done with a bootstrap test, but a permutation test is 
#preferred because it is more accurate (exact, in fact). But therein lies the 
#limit of a permutation test; it is not very versatile. We now want to test the 
#hypothesis that Frog A and Frog B have the same mean impact force, but not 
#necessarily the same distribution. This, too, is impossible with a permutation 
#test.

#To do the two-sample bootstrap test, we shift both arrays to have the same mean,
#since we are simulating the hypothesis that their means are, in fact, equal. We 
#then draw bootstrap samples out of the shifted arrays and compute the difference 
#in means. This constitutes a bootstrap replicate, and we generate many of them. 
#The p-value is the fraction of replicates with a difference in means greater than
#or equal to what was observed.

#################################################################################
####### TEST A/B ################################################################
#################################################################################

## ejemplo para democratas y republicanos
def frac_yay_dems(dems, reps):
    """Compute fraction of Democrat yay votes."""
    frac = np.sum(dems) / len(dems)
    return frac

##################################################################################
### Como hacer una prueba estadística para testear la correlación de dos variables
#################################################################################

#1) Proponer la Ho de que no están correlacionadas
#2) Simular los datos asumiendo que la hipótesis nula es cierta.
#3) Usar la correlación de pearson como prueba estadística.
#4) Computar el p-value como  una fracción de las replicas que tiene un pearson al menos
# tan grande como el observado.

## Para hacer la prueba estadística solo se necesita permutar una variable excluyendo 
## la otra
## Do a permutation test: Permute the illiteracy values but leave the fertility
# values fixed to generate a new set of (illiteracy, fertility) data.


## Creemos la función que hace la correlación de pearson
def pearson_r(x, y):
    """Compute Pearson correlation coefficient between two arrays."""
    # Compute correlation matrix: corr_mat
    corr_mat=np.corrcoef(x,y)

    # Return entry [0,1]
    return corr_mat[0,1]

# Compute Pearson correlation coefficient for I. versicolor: r

r=pearson_r(versicolor_petal_length,versicolor_petal_width)
# Print the result
print(r)

# Compute observed correlation: r_obs
r_obs = pearson_r(illiteracy, fertility)

# Initialize permutation replicates: perm_replicates
perm_replicates = np.empty(10000)

# Draw replicates
for i in range(10000):
    # Permute illiteracy measurments: illiteracy_permuted
    illiteracy_permuted = np.random.permutation(illiteracy)

    # Compute Pearson correlation
    perm_replicates[i] = pearson_r(illiteracy_permuted,fertility)

# Compute p-value: p
p = np.sum(perm_replicates>=r_obs)/len(perm_replicates)
print('p-val =', p)




    







