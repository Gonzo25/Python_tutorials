from bs4 import BeautifulSoup
import requests
import re
import pandas as pd
#from IPython.display import Image
#https://jarroba.com/scraping-python-beautifulsoup-ejemplos/
#http://web.stanford.edu/~zlotnick/TextAsData/Web_Scraping_with_Beautiful_Soup.html
#https://imasters.com.br/desenvolvimento/aprendendo-sobre-web-scraping-em-python-utilizando-beautifulsoup/?trace=1519021197&source=single




# Función que devueve texto a limpiar si lo encuentra
def search_text(txt,pattern):
    if re.search(pattern, txt):
        return re.search(pattern, txt).group()
    else:
        return ''

#  Función que máximiza la cantidad de páginas en cada especialidad

def max_paginas(html):
    list_li = []
    max_num_page=0

    for ultag in html.find_all('ul', {'id': 'pagination_search'}):
        for litag in ultag.find_all('li'):
            list_li.append(litag.text)
        max_num_page = list_li[len(list_li) - 2]
    return int(max_num_page)




# Convertimos a listas

#especialidades1=especialidades.values.tolist()

#1) Importamos el archivo de las Especilidades

especialidades=pd.read_csv('Especialidades_medicas.csv', header=None,encoding='latin1', index_col=False)


# Hacemos un loop para cada especialidad y dentro de ella se hace la extraccion para cada especialidad


cantidad_especialidades=len(especialidades)
lista_total=[] # Aquí es la lista temporal de cada especialidad

for count in range(1,cantidad_especialidades):
    url = 'http://www.svmed.ch/resultats-trouver-un-medecin/?Speciality='+especialidades.iloc[count,].get_value(0)+'&page=1'
    # Obtenemos la página
    page = requests.get(url)

    if (page.status_code ==200):
        html = BeautifulSoup(page.content, 'html5lib')
        #print(count)
        cantidad_paginas_html=max_paginas(html)+1


        for page_i in range(1, cantidad_paginas_html):
            url2 = 'http://www.svmed.ch/resultats-trouver-un-medecin/?Speciality=' + especialidades.iloc[count,].get_value(
                0) + '&page=' + str(page_i)
            # Obtenemos la página
            page2 = requests.get(url2)

            if(page2.status_code==200):
                html_page_i = BeautifulSoup(page2.content, 'html5lib')

                doctores_page = html_page_i.find_all('div', {'class': 'result_doctor'})
                especialistas_page_i=len(doctores_page)

                for especialista in range(1,especialistas_page_i):
                    entradas = html_page_i.find_all('div', {'class': 'result_doctor'})[especialista].getText()
                    txt = re.sub('\n|\t', "", entradas)  # quitamos los \n y \t
                    txt = re.sub('\s+', ' ', txt)
                    # extraccion nombre
                    name = search_text(txt, 'Prénom: \w+ \w+')
                    name = re.sub('Prénom: ', '', name)

                    # Extraccion de Rue
                    rue = search_text(txt, r'Rue: .* Localité')
                    rue = re.sub(r'^Rue: | Localité$', '', rue)
                    # extraccion de Localite
                    local = search_text(txt, r'Localité: .*Poste')
                    local = re.sub(r'Localité: |Poste', '', local)
                    # Extraccion de fixe
                    fixe = search_text(txt, r'fixe: \+[0-9]+')
                    fixe = re.sub(r'fixe: ', '', fixe)
                    # Extraccion de Nate1
                    nate = search_text(txt, r'Natel: \+[0-9]+')
                    nate = re.sub(r'^Natel: ', '', nate)

                    # Extraccion de telefax
                    telec = search_text(txt, r'Télécopie: \+[0-9]+')
                    telec = re.sub(r'Télécopie: ', '', telec)

                    # Extraccion de Disciplina
                    disciplina = search_text(txt, 'Disciplines.* [\w+ ]?')
                    disciplina = re.sub('^Disciplines', '', disciplina)

                    # Extraccion Especialidad
                    especialidad = search_text(txt, 'Spécialisations.* [\w+ ]?$')
                    especialidad = re.sub('^Spécialisations \(attestations de formation complémentaire\)', '',
                                          especialidad)

                    # creamos dictionary
                    # dict_doctor={'prenom':name,'Rue':rue,'Location':local,'fixe':fixe,'natel':nate,'telec':telec,'discipline':disciplina,'especialidad':especialidad}
                    dict_total = [
                        {'prenom': name, 'Rue': rue, 'Location': local, 'fixe': fixe, 'natel': nate, 'telec': telec,
                         'discipline': disciplina, 'especialidad': especialidad}]
                    lista_total.append(pd.DataFrame(dict_total))

df = pd.concat(lista_total)
df.shape

df.to_csv('especialidades Norman.csv')
